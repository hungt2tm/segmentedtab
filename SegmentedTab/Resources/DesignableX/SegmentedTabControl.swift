//
//  CustomSegmentedControl.swift
//  SegmentedTab
//
//  Created by Đỗ Hưng on 23/11/2021.
//  1. Background UIView
//  2. Selector UIView
//  3. UIButtons

import UIKit

@IBDesignable
class SegmentedTabControl: UIControl {
    var buttons = [UIButton]()
    var selector: UIView!
    var selectedIndex = 0

    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var commaSeparatedTitles: String = "" {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var textColor: UIColor = .lightGray {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var selectorColor: UIColor = .darkGray {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var selectorTextColor: UIColor = .white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var selectorPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        buttons.removeAll()
        subviews.forEach { $0.removeFromSuperview() }
        
        let buttonTitles = commaSeparatedTitles.components(separatedBy: ",")
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonTapped(sender:)), for: .touchUpInside)
            buttons.append(button)
        }
        buttons.first?.setTitleColor(selectorTextColor, for: .normal)
        
        let selectorWidth = frame.width / CGFloat(buttonTitles.count)
        let minusPadding = selectorPadding * 2
        selector = UIView(frame: CGRect(x: selectorPadding, y: selectorPadding, width: selectorWidth - minusPadding, height: frame.height - minusPadding))
        selector.layer.cornerRadius = (frame.height - minusPadding)  / 2
        selector.backgroundColor = selectorColor
        addSubview(selector)
        
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        stack.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = frame.height / 2
        let selectorWidth = frame.width / CGFloat(buttons.count)
        let minusPadding = selectorPadding * 2
        selector.frame.size = CGSize(width: selectorWidth - minusPadding, height: frame.height - minusPadding)
        selector.layer.cornerRadius = (frame.height - minusPadding)  / 2
    }
    
    @objc func buttonTapped(sender: UIButton) {
        for (index, button) in buttons.enumerated() {
            button.setTitleColor(textColor, for: .normal)
            if button == sender {
                selectedIndex = index
                let selectorPosition = frame.width / CGFloat(buttons.count) * CGFloat(index) + selectorPadding
                UIView.animate(withDuration: 0.3) {
                    self.selector.frame.origin.x = selectorPosition
                }
                button.setTitleColor(selectorTextColor, for: .normal)
            }
        }
        sendActions(for: .valueChanged)
    }
}
